<?php

namespace Olla\GraphQLBundle\GraphQL\Relay\Node;

use Olla\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Olla\GraphQLBundle\Definition\Resolver\ResolverInterface;

final class NodeFieldResolver implements ResolverInterface, AliasedInterface
{
    public function __invoke($args, $context, $info, \Closure $idFetcherCallback)
    {
        return ['id' => 'city'] ;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases()
    {
        return ['__invoke' => 'relay_node_field'];
    }
}
