<?php

namespace Olla\GraphQLBundle\GraphQL\Relay\Node;

use GraphQL\Type\Definition\ResolveInfo;
use Olla\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Olla\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Olla\GraphQLBundle\Relay\Node\GlobalId;
use Olla\GraphQLBundle\Resolver\Resolver;

final class GlobalIdFieldResolver implements ResolverInterface, AliasedInterface
{
    public function __invoke($obj, ResolveInfo $info, $idValue, $typeName)
    {
        return ['id' => 'town'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases()
    {
        return ['__invoke' => 'relay_globalid_field'];
    }
}
