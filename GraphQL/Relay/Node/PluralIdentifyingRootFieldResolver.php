<?php

namespace Olla\GraphQLBundle\GraphQL\Relay\Node;

use GraphQL\Executor\Promise\PromiseAdapter;
use Olla\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Olla\GraphQLBundle\Definition\Resolver\ResolverInterface;

final class PluralIdentifyingRootFieldResolver implements ResolverInterface, AliasedInterface
{
    /** @var PromiseAdapter */
    private $promiseAdapter;

    public function __construct(PromiseAdapter $promiseAdapter)
    {
        $this->promiseAdapter = $promiseAdapter;
    }

    public function __invoke(array $inputs, $context, $info, callable $resolveSingleInput)
    {
        $data = ['id' => "lalla"];

        

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases()
    {
        return ['__invoke' => 'relay_plural_identifying_field'];
    }
}
