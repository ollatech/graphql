<?php

namespace Olla\GraphQLBundle\Generator;


use Olla\Platform\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use Olla\Platform\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface;
use Olla\Platform\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use Olla\Platform\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface;
use Doctrine\Common\Util\Inflector;
use GraphQL\Type\Definition\Type as GraphQLType;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Olla\GraphQLBundle\Operation\OperationPool;

class CrudGenerator 
{

	private $propertyNameCollectionFactory;
	private $propertyMetadataFactory;
	private $resourceNameCollectionFactory;
	private $resourceMetadataFactory;
	private $operationPool;

	public function __construct(
		PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory,PropertyMetadataFactoryInterface $propertyMetadataFactory, ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, ResourceMetadataFactoryInterface $resourceMetadataFactory, 
		OperationPool $operationPool)
	{
		$this->propertyNameCollectionFactory = $propertyNameCollectionFactory;
		$this->propertyMetadataFactory = $propertyMetadataFactory;
		$this->resourceNameCollectionFactory = $resourceNameCollectionFactory;
		$this->resourceMetadataFactory = $resourceMetadataFactory;
		$this->operationPool = $operationPool;
	}

	public function get()
	{
		$graphTypes = [];
		$queryFields = [];
		$mutationFields = [];
		foreach ($this->resourceNameCollectionFactory->create() as $resourceClass) {
			$resourceMetadata = $this->resourceMetadataFactory->create($resourceClass);
			$shortName = $resourceMetadata->getShortName();

			$graphTypes += $this->buildType($shortName, $resourceMetadata, $resourceClass);

			$queryFields += $this->buildQuery($shortName, $resourceMetadata, $resourceClass);
			$mutationFields += $this->buildMutation($shortName, $resourceMetadata, $resourceClass);
		}
		return [
		'types' => $graphTypes,
		'query' =>  $queryFields,
		'mutation' => $mutationFields
		];
	}
	private function buildQuery($shortName, $resourceMetadata, $resourceClass) {
		$types = [];
		$types[lcfirst($shortName)] = [
		'type' => $shortName,
		'args' => [
		'id' => ['type' => 'String']
		],
		'resolve' =>  [
		'type' => 'crud',
		'operation' => 'item',
		'shortName' => $shortName,
		'className' => $resourceClass
		],

		];
		$types[lcfirst(Inflector::pluralize($shortName))] = [
		'type' => $shortName.'Connection',
		'resolve' =>  [
		'type' => 'crud',
		'operation' => 'collection',
		'shortName' => $shortName,
		'className' => $resourceClass
		],
		'args' => [
		'first' => ['type' => 'Int!'],
		'after' => ['type' => 'String'],
		],
		];
		return $types;
	}
	private function buildMutation($shortName, $resourceMetadata, $resourceClass) {
		$types = [];
		$types['create'.$shortName] = [
		'type' => $shortName.'CreatePayload',
		'args' => [
		'input' => ['type' => $shortName.'Input'],
		],
		'resolve' =>  [
		'type' => 'crud',
		'operation' => 'create',
		'shortName' => $shortName,
		'className' => $resourceClass
		]
		];

		$types['update'.$shortName] = [
		'type' => $shortName.'UpdatePayload',
		'args' => [
		'input' => ['type' => $shortName.'Input'],
		],
		'resolve' =>  [
		'type' => 'crud',
		'operation' => 'update',
		'shortName' => $shortName,
		'className' => $resourceClass
		]
		];

		$types['delete'.$shortName] = [
		'type' => $shortName.'DeletePayload',
		'args' => [
		'input' => [
			'id' => [
				'type' => 'String!',
				'description' => null
			]
		],
		'resolve' =>  [
		'type' => 'crud',
		'operation' => 'delete',
		'shortName' => $shortName,
		'className' => $resourceClass
		]
		];
		return $types;
	}

	private function buildType($shortName, $resourceMetadata, $resourceClass ) {

		$item = $this->getItemType($shortName, $resourceMetadata, $resourceClass);
		$input = $this->getInputType($shortName, $resourceMetadata, $resourceClass);
		$types = [];

		$types[$shortName]  = [
		'type' => 'object',
		'config' => ['fields' => $item]
		];

		$types[$shortName.'Connection']  = [
		'type' => 'relay-connection',
		'config' => [
		'nodeType' => $shortName
		]];

		$types[$shortName.'Input'] = [
		'type' => 'relay-mutation-input',
		'config' => ['fields' => $input]];

		$types[$shortName'CreatePayload'] = [
		'type' => 'relay-mutation-payload',
		'config' => [
		'fields' => ['result' =>[ 'type' => $shortName ]]
		]];


		$types[$shortName.'UpdatePayload'] = [
		'type' => 'relay-mutation-payload',
		'config' => [
		'fields' => ['result' =>[ 'type' => $shortName ]]
		]];

		$types[$shortName.'DeletePayload'] = [
		'type' => 'relay-mutation-payload',
		'config' => [
		'fields' => ['result' =>[ 'type' => $shortName ]]
		]];
		return $types;
	}

	private function getItemType($shortName, $resourceMetadata, $resourceClass) {
		$fieldDescription = null;
		$type = new Type(Type::BUILTIN_TYPE_OBJECT, true, $resourceClass);
		$rootResource = $resourceClass;
		$isInput = false;
		$isMutation = false;
		$mutationName = null;
		try {
			$className = $type->isCollection() ? $type->getCollectionValueType()->getClassName() : $type->getClassName();
			$originFields =  $this->getFields($type, $className);


			$fields = [];
			foreach ($originFields as $key => $value) {
				if($value) {
					$fields[$key] = $value;
				}
			}
			return $fields;
		} catch (InvalidTypeException $e) {
			return null;
		}
	}
	private function getInputType($shortName, $resourceMetadata, $resourceClass) {
		$fieldDescription = null;
		$type = new Type(Type::BUILTIN_TYPE_OBJECT, true, $resourceClass);
		$rootResource = $resourceClass;
		$isInput = false;
		$isMutation = false;
		$mutationName = null;
		try {
			$className = $type->isCollection() ? $type->getCollectionValueType()->getClassName() : $type->getClassName();
			$originFields =  $this->getInputFields($type, $className);
			$fields = [];
			foreach ($originFields as $key => $value) {
				if($value) {
					$fields[$key] = $value;
				}
			}
			return $fields;
		} catch (InvalidTypeException $e) {
			return null;
		}
	}

	

	private function getInputFields(Type $type, string $resource): array
	{
		$fields = [];
		foreach ($this->propertyNameCollectionFactory->create($resource) as $property) {
			$propertyMetadata = $this->propertyMetadataFactory->create($resource, $property);
			if (null === ($propertyType = $propertyMetadata->getType())) {
				continue;
			}
			if($this->getInputField($propertyType)) {
				$fields[$property] = $this->getInputField($propertyType);
			}
		}
		return $fields;
	}


	private function getInputField(Type $type)
	{
		try {
			$field =  $this->convertType($type, true);
			return $field;
		} catch (InvalidTypeException $e) {
			return null;
		}
	}


	
	

	private function getFields(Type $type, string $resource): array
	{
		$fields = [];
		foreach ($this->propertyNameCollectionFactory->create($resource) as $property) {
			$propertyMetadata = $this->propertyMetadataFactory->create($resource, $property);
			if (null === ($propertyType = $propertyMetadata->getType())) {
				continue;
			}
			if($this->getField($propertyType)) {
				$fields[$property] = $this->getField($propertyType);
			}
		}
		return $fields;
	}
	private function getField(Type $type)
	{
		try {
			$field =  $this->convertType($type);
			return $field;
		} catch (InvalidTypeException $e) {
			return null;
		}
	}
	
	private function convertType(Type $type, $isInput = false)
	{
		$isSubresource = false;
		$graphqlType = null;
		switch ($type->getBuiltinType()) {
			case Type::BUILTIN_TYPE_BOOL:
			$graphqlType = GraphQLType::boolean();
			break;
			case Type::BUILTIN_TYPE_INT:
			$graphqlType = GraphQLType::int();
			break;
			case Type::BUILTIN_TYPE_FLOAT:
			$graphqlType = GraphQLType::float();
			break;
			case Type::BUILTIN_TYPE_STRING:
			$graphqlType = GraphQLType::string();
			break;
			case Type::BUILTIN_TYPE_OBJECT:
			if (is_a($type->getClassName(), \DateTimeInterface::class, true)) {
				$graphqlType = GraphQLType::string();
				break;
			}
			try {
				$className = $type->isCollection() ? $type->getCollectionValueType()->getClassName() : $type->getClassName();
			} catch (ResourceClassNotFoundException $e) {
				throw new InvalidTypeException();
			}
			$isSubresource = true;
			if($isInput) {
				return $this->getSubresourceInput($type->isCollection(), $className);
			} else {
				return $this->getSubresource($type->isCollection(), $className);
			}
			
			break;
			default:
			throw new InvalidTypeException();
		}

	
		if($graphqlType && isset($graphqlType->name)) {
			return [
			'type' => $graphqlType->name,
			'description' => null
			];
		}
		return null;
	}

	private function getSubresourceInput($isColelction, $className) {
		$shortName = null;
		$childName = null;
		$childClass = null;
		$isFound = false;
		foreach ($this->resourceNameCollectionFactory->create() as $resourceClass) {
			$resourceMetadata = $this->resourceMetadataFactory->create($resourceClass);
			$findName = $resourceMetadata->getShortName();
			if($resourceClass === $className) {
				$resourceMetadata = $this->resourceMetadataFactory->create($className);
				$shortName = $resourceMetadata->getShortName();
				$childName = $findName;
				$childClass = $resourceClass;
				$isFound = true;
			}
		}
		if(!$isFound) {
			return null;
		}
		if($isColelction) {
		} else {
			return [
			'type' => $childName.'Input',
			'description' => null
			];
		}

	}


	private function getSubresource($isColelction, $className) {
		$shortName = null;
		$childName = null;
		$childClass = null;
		$isFound = false;
		foreach ($this->resourceNameCollectionFactory->create() as $resourceClass) {
			$resourceMetadata = $this->resourceMetadataFactory->create($resourceClass);
			$findName = $resourceMetadata->getShortName();
			if($resourceClass === $className) {
				$resourceMetadata = $this->resourceMetadataFactory->create($className);
				$shortName = $resourceMetadata->getShortName();
				$childName = $findName;
				$childClass = $resourceClass;
				$isFound = true;
			}
		}
		//echo $childClass;
		if(!$isFound) {
			return null;
		}
		if($isColelction) {
			return [
			'type' => $childName.'Connection',
			'description' => "Sub resource",
			'args' => [
				'first' => ['type' => 'Int'],
				'after' => ['type' => 'String']
			],
			'resolve' =>  [
			'type' => 'crud',
			'operation' => 'subresource',
			'parentName' => $shortName,
			'parentClass' => $resourceClass,
			'shortName' => $childName,
			'className' => $childClass
			]
			];
		} else {
			return [
			'type' => $childName,
			'description' => "Sub resource",
			'resolve' =>  [
			'type' => 'crud',
			'operation' => 'subitem',
			'parentName' => $shortName,
			'parentClass' => $resourceClass,
			'shortName' => $childName,
			'className' => $childClass
			]
			];
		}

	}
}
