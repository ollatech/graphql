<?php

namespace Olla\GraphQLBundle\Definition\Builder;

interface MappingInterface
{
    /**
     * @param array $config
     *
     * @return array
     */
    public function toMappingDefinition(array $config);
}
