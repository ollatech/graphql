<?php

namespace Olla\GraphQLBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GraphiQLController extends Controller
{
    public function indexAction($schemaName = null)
    {
        if (null === $schemaName) {
            $endpoint = $this->generateUrl('olla_graphql_endpoint');
        } else {
            $endpoint = $this->generateUrl('olla_graphql_multiple_endpoint', ['schemaName' => $schemaName]);
        }

        return $this->render(
            $this->getParameter('olla_graphql.graphiql_template'),
            [
                'endpoint' => $endpoint,
                'versions' => [
                    'graphiql' => $this->getParameter('olla_graphql.versions.graphiql'),
                    'react' => $this->getParameter('olla_graphql.versions.react'),
                    'fetch' => $this->getParameter('olla_graphql.versions.fetch'),
                ],
            ]
        );
    }
}
