<?php

namespace Olla\GraphQLBundle\Event;

final class Events
{
    const EXECUTOR_CONTEXT = 'graphql.executor.context';
}
