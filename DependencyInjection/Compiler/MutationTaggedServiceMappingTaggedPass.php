<?php

namespace Olla\GraphQLBundle\DependencyInjection\Compiler;

class MutationTaggedServiceMappingTaggedPass extends ResolverTaggedServiceMappingPass
{
    protected function getTagName()
    {
        return 'olla_graphql.mutation';
    }

    protected function getResolverServiceID()
    {
        return 'olla_graphql.mutation_resolver';
    }
}
