<?php

namespace Olla\GraphQLBundle\DependencyInjection\Compiler;

class TypeTaggedServiceMappingPass extends TaggedServiceMappingPass
{
    const TAG_NAME = 'olla_graphql.type';

    protected function getTagName()
    {
        return self::TAG_NAME;
    }

    protected function getResolverServiceID()
    {
        return 'olla_graphql.type_resolver';
    }
}
