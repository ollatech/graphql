<?php

namespace Olla\GraphQLBundle\Resolver;

class UnsupportedResolverException extends \InvalidArgumentException
{
}
