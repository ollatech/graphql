<?php

namespace Olla\GraphQLBundle\Resolver;

class UnresolvableException extends \InvalidArgumentException
{
}
