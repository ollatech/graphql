<?php

namespace Olla\GraphQLBundle\Error;

/**
 * Class UserFacingError.
 */
abstract class UserFacingError extends \Exception
{
}
