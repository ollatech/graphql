<?php
namespace Olla\GraphQLBundle\DataProvider\Orm;

use Doctrine\Common\Persistence\ManagerRegistry;

class CollectionDataProvider
{
    private $managerRegistry;
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }
    public function supports(string $resourceClass, string $operationName = null): bool
    {
        return null !== $this->managerRegistry->getManagerForClass($resourceClass);
    }
    public function getData(string $resourceClass, $limit = 10,  array $filters = [], $sortBy = 'id', $sort = 'ASC')
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        if (!method_exists($repository, 'createQueryBuilder')) {
            return;
        }
        $queryBuilder = $repository->createQueryBuilder('o');
        $queryBuilder->setMaxResults($limit);
        return [
            'result' => $queryBuilder->getQuery()->getResult(),
            'attributes' => [
                'total_items' => 100,
                'items_per_page' => 10,
                'current_page' => 1
            ]
        ];
    }
    private function getFilters() {

    }
}
