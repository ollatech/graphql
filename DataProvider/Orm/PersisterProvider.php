<?php
namespace Olla\GraphQLBundle\DataProvider\Orm;

use Doctrine\Common\Persistence\ManagerRegistry;
use Olla\GraphQLBundle\DataProvider\Serializer\ItemNormalizer;

class PersisterProvider
{
    private $managerRegistry;
    private $normalizer;
    public function __construct(ManagerRegistry $managerRegistry, ItemNormalizer $normalizer)
    {
        $this->managerRegistry = $managerRegistry;
        $this->normalizer = $normalizer;
    }
    
    public function persist($data, $resourceClass) {
         $item = null;

         if($data && isset($data['id'])) {
            //find item to update here
         }
         $context = null === $item ? ['resource_class' => $resourceClass] : ['resource_class' => $resourceClass, 'object_to_populate' => $item];
         $item = $this->normalizer->denormalize($data, $resourceClass, null, $context);
         //get manager


    }
    public function remove($data, $resourceClass) {

    }
}
