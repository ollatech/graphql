<?php
namespace Olla\GraphQLBundle\DataProvider\Orm;

use Doctrine\Common\Persistence\ManagerRegistry;

class SubitemDataProvider
{
    private $managerRegistry;
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }
    public function supports(string $resourceClass, string $operationName = null): bool
    {
        return null !== $this->managerRegistry->getManagerForClass($resourceClass);
    }

    public function getData(string $resourceClass, $id)
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        if (!method_exists($repository, 'createQueryBuilder')) {
            return;
        }
        $queryBuilder = $repository->createQueryBuilder('o');
        $queryBuilder->where('o.id = :id');
        $queryBuilder->setParameter('id', $id);
        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
