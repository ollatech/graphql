<?php

namespace Olla\GraphQLBundle\DataProvider\Serializer;

use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Doctrine\Common\Persistence\ManagerRegistry;


class ItemNormalizer extends ObjectNormalizer
{
	
	private $managerRegistry;

	public function __construct(ManagerRegistry $managerRegistry, NameConverterInterface $nameConverter = null, ClassMetadataFactoryInterface $classMetadataFactory = null)
	{
		parent::__construct($classMetadataFactory, $nameConverter);
		$this->managerRegistry = $managerRegistry;
		
	}

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
    	if (!is_object($data)) {
    		return false;
    	}
    	try {
    		$this->resourceClassResolver->getResourceClass($data);
    	} catch (InvalidArgumentException $e) {
    		return false;
    	}
    	return true;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
    	
    	if (isset($context['resources'])) {
    		
    	}

    	return parent::normalize($object, $format, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
    	return $this->localCache[$type] ?? $this->localCache[$type] = $this->resourceClassResolver->isResourceClass($type);
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
    	$context['api_denormalize'] = true;
    	if (!isset($context['resource_class'])) {
    		$context['resource_class'] = $class;
    	}
    	return parent::denormalize($data, $class, $format, $context);
    }



    /**
     * {@inheritdoc}
     */
    protected function getAllowedAttributes($classOrObject, array $context, $attributesAsString = false)
    {


        return $allowedAttributes;
    }

    /**
     * {@inheritdoc}
     */
    protected function setAttributeValue($object, $attribute, $value, $format = null, array $context = [])
    {
    	
    }

}