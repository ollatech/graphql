<?php

namespace Olla\GraphQLBundle\EventListener;

use Olla\GraphQLBundle\Generator\TypeGenerator;

class ClassLoaderListener
{
    private $typeGenerator;

    public function __construct(TypeGenerator $typeGenerator)
    {
        $this->typeGenerator = $typeGenerator;
    }

    public function load()
    {
        $this->typeGenerator->loadClasses();
    }
}
