<?php


declare(strict_types=1);
namespace Olla\GraphQLBundle\Operation;

use Olla\GraphQLBundle\DataProvider\ORM\CollectionDataProvider;
use Olla\GraphQLBundle\DataProvider\ORM\SubresourceDataProvider;
use Olla\GraphQLBundle\DataProvider\ORM\ItemDataProvider;
use Olla\GraphQLBundle\DataProvider\ORM\SubitemDataProvider;
use Olla\GraphQLBundle\DataProvider\ORM\PersisterProvider;
use Olla\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Olla\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Olla\GraphQLBundle\Definition\Argument;
use Olla\GraphQLBundle\Relay\Connection\Paginator;
use Symfony\Component\Serializer\Serializer;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Psr\Log\NullLogger;
 
class OperationPool
{

    private $logger;
	private $collectionProvider;
	private $subresourceProvider;
	private $itemProvider;
	private $subitemProvider;
	private $persisterProvider;
	public function __construct(
		CollectionDataProvider $collectionProvider,
		SubresourceDataProvider $subresourceProvider,
		ItemDataProvider $itemProvider,
		SubitemDataProvider $subitemProvider,
		PersisterProvider $persisterProvider,
		LoggerInterface $logger = null
		) {
		$this->logger = (null === $logger) ? new NullLogger() : $logger;
		$this->collectionProvider = $collectionProvider;
		$this->subresourceProvider = $subresourceProvider;
		$this->itemProvider = $itemProvider;
		$this->subitemProvider = $subitemProvider;
		$this->persisterProvider = $persisterProvider;
	}
	public function collection($className, $shortName, $value, $args, $context, $container, $request, $user, $token)
	{
		$first = isset($args['first']) ? $args['first'] : 1;
		$after = isset($args['after']) ? $args['after'] : null;
		$filters = isset($args['filters']) ? $args['filters'] : null;
		$fields = isset($args['fields']) ? $args['fields'] : null;
		$paginator = new Paginator(function ($offset) use ($className, $filters) {
			$request = $this->collectionProvider->getData($className);
			return $request['result'];
		});
		return $paginator->forward(new Argument(['first' => $first,'after' => $after]));
	}
 
	public function subresource($parentClass, $parentName,  $className, $shortName,  $value, $args, $context, $container, $request, $user, $token, $info)
	{
		$first = isset($args['first']) ? $args['first'] : 1;
		$after = isset($args['after']) ? $args['after'] : null;
		$filters = isset($args['filters']) ? $args['filters'] : null;
		$fields = isset($args['fields']) ? $args['fields'] : null;
		$paginator = new Paginator(function ($offset) use ($className, $filters) {
			$request = $this->collectionProvider->getData($className);
			return $request['result'];
		});
		return $paginator->forward(
			new Argument(['first' => $first,'after' => $after])
			);
	}
	public function item($className, $shortName, $value, $args, $context, $container, $request, $user, $token, $info)
	{
		return $this->itemProvider->getData($className, $args['id']);
	}
	public function subitem($parentClass, $parentName,  $className, $shortName,  $value, $args, $context, $container, $request, $user, $token, $info)
	{
		return [];
	}
	public function create($className, $shortName, $value, $args, $context, $container, $request, $user, $token, $info)
	{

		return [];
	}
	public function update($className, $shortName, $value, $args, $context, $container, $request, $user, $token, $info)
	{
		return [];
	}
	public function delete($className, $shortName, $value, $args, $context, $container, $request, $user, $token, $info)
	{
		return [];
	}
}