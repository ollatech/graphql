<?php

namespace Olla\GraphQLBundle;

use Olla\GraphQLBundle\DependencyInjection\Compiler\AutoMappingPass;
use Olla\GraphQLBundle\DependencyInjection\Compiler\AutowiringTypesPass;
use Olla\GraphQLBundle\DependencyInjection\Compiler\ConfigTypesPass;
use Olla\GraphQLBundle\DependencyInjection\Compiler\MutationTaggedServiceMappingTaggedPass;
use Olla\GraphQLBundle\DependencyInjection\Compiler\ResolverTaggedServiceMappingPass;
use Olla\GraphQLBundle\DependencyInjection\Compiler\TypeTaggedServiceMappingPass;
use Olla\GraphQLBundle\DependencyInjection\OllaGraphQLExtension;
use Olla\GraphQLBundle\DependencyInjection\OllaGraphQLTypesExtension;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class OllaGraphQLBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        //ConfigTypesPass and AutoMappingPass must be before TypeTaggedServiceMappingPass
        $container->addCompilerPass(new AutoMappingPass());
        $container->addCompilerPass(new ConfigTypesPass());
        $container->addCompilerPass(new AutowiringTypesPass());
        $container->addCompilerPass(new TypeTaggedServiceMappingPass(), PassConfig::TYPE_BEFORE_REMOVING);
        $container->addCompilerPass(new ResolverTaggedServiceMappingPass(), PassConfig::TYPE_BEFORE_REMOVING);
        $container->addCompilerPass(new MutationTaggedServiceMappingTaggedPass(), PassConfig::TYPE_BEFORE_REMOVING);
        $container->registerExtension(new OllaGraphQLTypesExtension());
    }

    public function getContainerExtension()
    {
        if (!$this->extension instanceof ExtensionInterface) {
            $this->extension = new OllaGraphQLExtension();
        }

        return $this->extension;
    }
}
