<?php

namespace Olla\GraphQLBundle\ExpressionLanguage;

use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security\HasAnyPermission;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security\HasAnyRole;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security\HasPermission;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security\HasRole;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security\IsAnonymous;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security\IsAuthenticated;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security\IsFullyAuthenticated;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security\IsRememberMe;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;

class AuthorizationExpressionProvider implements ExpressionFunctionProviderInterface
{
    public function getFunctions()
    {
        return [
            new HasRole(),
            new HasAnyRole(),
            new IsAnonymous(),
            new IsRememberMe(),
            new IsFullyAuthenticated(),
            new IsAuthenticated(),
            new HasPermission(),
            new HasAnyPermission(),
        ];
    }
}
