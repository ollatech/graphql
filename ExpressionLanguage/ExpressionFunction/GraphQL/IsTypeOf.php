<?php

namespace Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL;

use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction;

final class IsTypeOf extends ExpressionFunction
{
    public function __construct($name = 'isTypeOf')
    {
        parent::__construct(
            $name,
            function ($className) {
                return sprintf('($className = %s) && $value instanceof $className', $className);
            }
        );
    }
}
