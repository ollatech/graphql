<?php

namespace Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Relay;

use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction;
use Olla\GraphQLBundle\Generator\TypeGenerator;

final class MutateAndGetPayloadCallback extends ExpressionFunction
{
    public function __construct($name = 'mutateAndGetPayloadCallback')
    {
        parent::__construct(
            $name,
            function ($mutateAndGetPayload) {
                $code = 'function ($value) use ('.TypeGenerator::USE_FOR_CLOSURES.', $args, $context, $info) { ';
                $code .= 'return '.$mutateAndGetPayload.'; }';

                return $code;
            }
        );
    }
}
