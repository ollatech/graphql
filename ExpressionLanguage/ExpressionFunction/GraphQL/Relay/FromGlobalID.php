<?php

namespace Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Relay;

use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction;

final class FromGlobalID extends ExpressionFunction
{
    public function __construct($name = 'fromGlobalId')
    {
        parent::__construct(
            $name,
            function ($globalId) {
                return sprintf(
                    '\%s::fromGlobalId(%s)',
                    \Olla\GraphQLBundle\Relay\Node\GlobalId::class,
                    $globalId
                );
            }
        );
    }
}
