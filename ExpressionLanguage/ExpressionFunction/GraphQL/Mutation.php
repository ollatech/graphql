<?php

namespace Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL;

use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction;

final class Mutation extends ExpressionFunction
{
    public function __construct($name = 'mutation')
    {
        parent::__construct(
            $name,
            function ($alias, $args = '[]') {
                return sprintf('$container->get(\'olla_graphql.mutation_resolver\')->resolve([%s, %s])', $alias, $args);
            }
        );
    }
}
