<?php

namespace Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\Security;

use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction;

final class IsFullyAuthenticated extends ExpressionFunction
{
    public function __construct($name = 'isFullyAuthenticated')
    {
        parent::__construct(
            $name,
            function () {
                return '$container->get(\'security.authorization_checker\')->isGranted(\'IS_AUTHENTICATED_FULLY\')';
            }
        );
    }
}
