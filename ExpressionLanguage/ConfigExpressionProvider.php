<?php

namespace Olla\GraphQLBundle\ExpressionLanguage;

use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\DependencyInjection\Parameter;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\DependencyInjection\Service;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\IsTypeOf;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Mutation;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Relay\FromGlobalID;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Relay\GlobalID as GlobalIDFunction;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Relay\IdFetcherCallback;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Relay\MutateAndGetPayloadCallback;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Relay\ResolveSingleInputCallback;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\GraphQL\Resolver;
use Olla\GraphQLBundle\ExpressionLanguage\ExpressionFunction\NewObject;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;

class ConfigExpressionProvider implements ExpressionFunctionProviderInterface
{
    public function getFunctions()
    {
        return [
            new Service(),
            new Service('serv'),
            new Parameter(),
            new Parameter('param'),
            new IsTypeOf(),
            new Resolver(),
            new Resolver('res'),
            new Mutation(),
            new Mutation('mut'),
            new MutateAndGetPayloadCallback(),
            new IdFetcherCallback(),
            new ResolveSingleInputCallback(),
            new GlobalIDFunction(),
            new FromGlobalID(),
            new NewObject(),
        ];
    }
}
